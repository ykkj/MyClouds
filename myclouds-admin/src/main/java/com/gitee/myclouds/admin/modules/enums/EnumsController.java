package com.gitee.myclouds.admin.modules.enums;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 枚举参数 服务发布
 * 
 * @author xiongchun
 *
 */
@RestController
@RequestMapping("enums")
public class EnumsController {

}
